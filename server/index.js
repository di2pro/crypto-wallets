const { promisify } = require('util');
const bodyParser = require('body-parser');
const express = require('express');
const mongoose = require('mongoose');
const { createClient } = require('redis');

const api = require('./api/routes');
const balanceService = require('./api/services/balance');
const app = express();

const { MONGO_URI, REDIS_HOST, REDIS_PORT, PORT = 3001 } = process.env;

const bootstrap = async () => {
  try {
    redisClient = await createClient(REDIS_PORT, REDIS_HOST);
    balanceService.init(redisClient);

    db = await mongoose.connect(MONGO_URI, { useNewUrlParser: true });

    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
    app.use('/', api);

    await promisify(app.listen.bind(app))(PORT);
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

bootstrap();
