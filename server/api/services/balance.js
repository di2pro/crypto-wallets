const { promisify } = require('util');

const PREFIX_RESERVED_BALANCES = 'balance:reserved:';
const PREFIX_MARGIN_COLLATERAL = 'margin:collateral:';
const balanceService = exports;

let client;

const convertResultValuesToNumbers = (result) => {
  if (!result) {
    return {};
  }

  return Object.keys(result).reduce((map, currency) => {
    map[currency] = +result[currency];

    return map;
  }, {});
};

balanceService.fetchReservedBalancesFromRedis = async (userId) => {
  try {
    balances = await promisify(client.hgetall.bind(client))(PREFIX_RESERVED_BALANCES + userId)
    return convertResultValuesToNumbers(balances)
  } catch (error) {
    return {};
  }
};

balanceService.updateCollateralInRedis = async (userId, values) => {
  try {
    return await promisify(client.hmset.bind(client))(PREFIX_MARGIN_COLLATERAL + userId, values);
  } catch (error) {
    return {};
  }
};

balanceService.init = (redisClient) => {
  client = redisClient
};
