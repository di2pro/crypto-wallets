const Wallet = require('../models/wallet');
const { fetchReservedBalancesFromRedis, updateCollateralInRedis } = require('../services/balance');

module.exports = {
  fetchAllBalances: (req, res) => {
    const {userId} = req.params;

    Wallet.find({ userId })
      .exec()
      .then((wallets) => {
        const balances = wallets.reduce((map, { currency, balance }) => {
          map[currency] = balance;

          return map;
        }, {});

        res.status(200);
        res.json(balances);
      })
      .catch((error) => {
        res.status(500);
        res.json(error);
      });
  },

  fetchPrices: (req, res) => {
    res.status(200);
    res.json({
      BTC: 6000,
      ETH: 300,
      LTC: 200,
      USD: 1,
      XRP: 0.5
    });
  },

  fetchReservedBalances: (req, res) => {
    const { userId } = req.params;

    fetchReservedBalancesFromRedis(userId)
      .then((reservedBalances) => {
        res.status(200);
        res.json(reservedBalances);
      })
      .catch((error) => {
        res.status(500);
        res.json(error);
      });
  },

  updateCollateral: (req, res) => {
    const { userId } = req.params;
    const updatedCollateral = req.body;

    return Promise.all([
      Wallet.find({ userId }).exec(),
      fetchReservedBalancesFromRedis(userId),
    ])
      .then(([ wallets, reservedBalances ]) => {
        const currencies = wallets.map(({ currency }) => currency);
        const balances = wallets.map(({ currency, balance }) => ({ [currency]: balance }));

        let isUpdatedCollateralBiggerThanBalance = false;

        currencies.forEach(currency => {
          if (balances[currency] < reservedBalances[currency] + updatedCollateral[currency]) {
            isUpdatedCollateralBiggerThanBalance = true;
          }
        });

        if (isUpdatedCollateralBiggerThanBalance) {
          return res.status(400);
        }

        const data = Object.keys(updatedCollateral).reduce((array, key) => {
          array.push(key);
          array.push(updatedCollateral[key]);

          return array;
        }, []);

        return updateCollateralInRedis(userId, data);
      })
      .then(() => {
        res.status(201);
        res.json({});
      })
      .catch((error) => {
        res.status(500);
        res.json(error);
      });
  },
};
