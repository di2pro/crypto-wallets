const { Router } = require('express');

const WalletCtrl = require('./controllers/wallet-controller');

const router = Router();
router.get('/:userId/all_balances', WalletCtrl.fetchAllBalances);
router.get('/:userId/reserved_balances', WalletCtrl.fetchReservedBalances);
router.get('/:userId/prices', WalletCtrl.fetchPrices);
router.post('/:userId/collateral', WalletCtrl.updateCollateral);
module.exports = router;
