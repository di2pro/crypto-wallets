import React, { PureComponent } from 'react';

import './App.css';

import WalletCardsContainer from './containers/WalletCardsContainer';
import UpdateCollateralButtonContainer from './containers/UpdateCollateralButtonContainer';

class App extends PureComponent {
  render() {
    return (
      <div className="crypto-wallet-app">
        <WalletCardsContainer />
        <UpdateCollateralButtonContainer/>
      </div>
    );
  }
}

export default App;
