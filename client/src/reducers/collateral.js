const VALUE_UPDATED = 'COLLATERAL_VALUE_UPDATED';

export default function collateral(state = {}, { type, payload }) {
  switch (type) {
    case VALUE_UPDATED:
      return {
        ...state,
        [payload.currency]: payload.value
      };

    default:
      return state;
  }
}

export const updateCollateral = () => (dispatch, getState) => {
  const { user, collateral } = getState();

  return fetch(`/${user._id}/collateral`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(collateral),
  })
    .then(response => response.json())
    .then(response => console.info(response));
};

export const updateCollateralValue = (value, currency) => ({
  type: VALUE_UPDATED,
  payload: { value, currency }
});
