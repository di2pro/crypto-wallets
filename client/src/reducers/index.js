import { combineReducers } from 'redux';

import balances, { fetchBalances } from './balances';
import collateral, { updateCollateralValue } from './collateral';
import prices, { fetchPrices } from './prices';
import reservedBalances, { fetchReservedBalances } from './reserved-balances';
import user from './user';

const cryptoWalletApp = combineReducers({
  user,
  balances,
  collateral,
  prices,
  reservedBalances
});

export default cryptoWalletApp;

export const fetchAllData = () => (dispatch, getState) => {
  const { user } = getState();

  dispatch(fetchBalances(user._id));
  dispatch(fetchReservedBalances(user._id));
  dispatch(fetchPrices(user._id));
};

export const updateWalletValue = (value, currency) => (dispatch) => {
  return dispatch(updateCollateralValue(value, currency));
};
