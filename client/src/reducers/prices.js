const FETCHED_SUCCESS = 'PRICES_FETCHED_SUCCESS';

export default function prices(state = {}, action) {
  switch (action.type) {
    case FETCHED_SUCCESS:
      return action.payload;
    default:
      return state;
  }
}

const receivePrices = (prices) => ({
  type: FETCHED_SUCCESS,
  payload: prices
});

export const fetchPrices = (userId) => (dispatch) => {
  return fetch(`/${userId}/prices`)
    .then(response => response.json())
    .then(prices => dispatch(receivePrices(prices)));
};
