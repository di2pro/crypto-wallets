const FETCHED_SUCCESS = 'RESERVED_BALANCES_FETCHED_SUCCESS';

export default function reservedBalances(state = {}, { type, payload }) {
  switch (type) {
    case FETCHED_SUCCESS:
      return payload;

    default:
      return state;
  }
}

const receiveReservedBalances = (balances) => ({
  type: FETCHED_SUCCESS,
  payload: balances
});

export const fetchReservedBalances = (userId) => (dispatch) => {
  return fetch(`/${userId}/reserved_balances`)
    .then(response => response.json())
    .then(balances => dispatch(receiveReservedBalances(balances)));
};
