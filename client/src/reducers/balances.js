const FETCHED_SUCCESS = 'BALANCES_FETCHED_SUCCESS';

export default function balances(state = {}, action) {
  switch (action.type) {
    case FETCHED_SUCCESS:
      return action.payload;
    default:
      return state;
  }
}

const receiveBalances = (balances) => ({
  type: FETCHED_SUCCESS,
  payload: balances
});

export const fetchBalances = (userId) => (dispatch) => {
  return fetch(`/${userId}/all_balances`)
    .then(response => response.json())
    .then(json => dispatch(receiveBalances(json)));
};
