import { connect } from 'react-redux';

import UpdateCollateralButton from '../components/UpdateCollateralButton/UpdateCollateralButton';
import { updateCollateral } from '../reducers/collateral'

const mapStateToProps = (state) => state;
const mapDispatchToProps = (dispatch) => ({
  onButtonClick: () => dispatch(updateCollateral())
});

const UpdateCollateralButtonContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UpdateCollateralButton);

export default UpdateCollateralButtonContainer;
