import { connect } from 'react-redux';

import WalletCards from '../components/WalletCards/WalletCards';
import { fetchAllData, updateWalletValue } from '../reducers';

const mapStateToProps = ({ user, balances, collateral, reservedBalances, prices }) => ({
  balances,
  collateral,
  reservedBalances,
  prices
});

const mapDispatchToProps = (dispatch) => ({
  fetchAllData: () => dispatch(fetchAllData()),
  updateInputValue: (value, currency) => dispatch(updateWalletValue(value, currency))
});

const WalletCardsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(WalletCards);

export default WalletCardsContainer;
