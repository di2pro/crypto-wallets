import React from 'react';
import PropTypes from 'prop-types';

import './update-collateral-button.css';

export default function UpdateCollateralButton({ onButtonClick }) {
  return (
    <button
      type="button"
      onClick={onButtonClick}
      className="update-collateral-btn"
    >
      Update
    </button>
  );
}

UpdateCollateralButton.propTypes = {
  onButtonClick: PropTypes.func.isRequired,
};
