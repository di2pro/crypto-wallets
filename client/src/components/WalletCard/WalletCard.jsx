import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './wallet-card.css';
import walletIcon from './wallet-solid.svg';

export default class WalletCard extends PureComponent {
  constructor(props) {
    super(props);

    const { balance, reservedBalance } = props;

    this.state = {
      maxCollateralValue: balance - reservedBalance
    };

    this.onChangeCollateral = this.onChangeCollateral.bind(this);
    this.onChangeCollateralInFiat = this.onChangeCollateralInFiat.bind(this);
  }

  componentDidMount() {
    const { maxCollateralValue } = this.state;
    const { updateInputValue, currency } = this.props;

    updateInputValue(maxCollateralValue, currency);
  }

  componentWillReceiveProps(nextProps) {
    const { balance, reservedBalance, currency, updateInputValue } = this.props;

    if (balance !== nextProps.balance || reservedBalance !== nextProps.reservedBalance) {
      const maxCollateralValue = nextProps.balance - nextProps.reservedBalance;

      this.setState(() => ({ maxCollateralValue }), () => updateInputValue(maxCollateralValue, currency));
    }
  }

  onChangeCollateral(event) {
    const { maxCollateralValue } = this.state;
    const { updateInputValue, currency } = this.props;

    let value = parseInt(event.target.value, 10);
    value = isNaN(value) ? 0 : value;

    if (value > maxCollateralValue) {
      return;
    }

    return updateInputValue(value, currency);
  }

  onChangeCollateralInFiat(event) {
    const { maxCollateralValue } = this.state;
    const { updateInputValue, currency, price } = this.props;

    let value = parseInt(event.target.value, 10);
    value = isNaN(value) ? 0 : value / price;

    if (value > maxCollateralValue) {
      return;
    }

    return updateInputValue(value, currency);
  }

  render() {
    const {
      balance,
      collateral,
      reservedBalance,
      currency,
      price
    } = this.props;

    return (
      <div className="wallet-card">
        <div className="wallet-card__head">
          <h3 className="wallet-card__currency">{currency}</h3>
          <div className="wallet-card__collateral">
            <input
              className="wallet-card__collateral-input"
              type="text"
              value={collateral}
              onChange={this.onChangeCollateral}
            />
          </div>
        </div>
        <div className="wallet-card__progress-bar-container">
          <div className="wallet-card__progress-bar-value">
            {Math.round((reservedBalance + collateral) / balance * 100)}%
          </div>
        </div>
        <div className="wallet-card__balances">
          <div className="wallet-card__balance">
            <img className="wallet-card__icon" src={walletIcon} alt="wallet"/>
            {balance * price}
          </div>
          <div className="wallet-card__reserved-balance">
            <input
              className="wallet-card__reserved-balance-input"
              type="text"
              value={collateral * price}
              onChange={this.onChangeCollateralInFiat}
            />
          </div>
        </div>
      </div>
    );
  }
}

WalletCard.propTypes = {
  balance: PropTypes.number,
  collateral: PropTypes.number,
  reservedBalance: PropTypes.number,
  currency: PropTypes.string,
  price: PropTypes.number,
  updateInputValue: PropTypes.func.isRequired,
};

WalletCard.defaultProps = {
  balance: 0,
  collateral: 0,
  reservedBalance: 0,
  currency: '',
  price: 0,
};
