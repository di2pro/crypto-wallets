import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './wallet-cards.css';
import WalletCard from '../WalletCard/WalletCard';

export default class WalletCards extends PureComponent {
  componentDidMount() {
    const { fetchAllData } = this.props;

    fetchAllData();
  }

  render() {
    const { balances, collateral, prices, reservedBalances, updateInputValue } = this.props;

    return (
      <div className="wallet-cards">
        {Object.keys(balances).map((currency, index) => (
          <WalletCard
            key={index}
            currency={currency}
            collateral={collateral[currency]}
            balance={balances[currency]}
            reservedBalance={reservedBalances[currency]}
            price={prices[currency]}
            updateInputValue={updateInputValue}
          />
        ))}
      </div>
    );
  }
}

WalletCards.propTypes = {
  balances: PropTypes.object.isRequired,
  collateral: PropTypes.object.isRequired,
  reservedBalances: PropTypes.object.isRequired,
  prices: PropTypes.object.isRequired,
  fetchAllData: PropTypes.func.isRequired,
  updateInputValue: PropTypes.func.isRequired,
};
